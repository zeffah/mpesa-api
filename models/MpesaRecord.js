var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var MpesaRecordSchema = new Schema({
    text: { type: String },
});

module.exports = mongoose.model("Mpesa", MpesaRecordSchema);
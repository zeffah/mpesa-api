var express = require('express')
var router = express.Router()
var mysql = require('mysql')
const fs = require('fs')
const Mpesa = require('../models/MpesaRecord')

router.post('/', (req, res, next) => {
    console.log(req.body)
    var mpesa = new Mpesa({ text: JSON.stringify(req.body) })
    mpesa.save().then(mpesa => {
        res.json({ ResultCode: 0, ResultDesc: "Confirmation received successfully" })
    })
    .catch(error => {
        throw error
    })
})

module.exports = router;
require('dotenv').config()
var express = require('express');
var router = express.Router();
const {getOathToken, c2bRegisterUrl, c2bSimulateUrl, mPesaSTKPush} = require('../mpesa-config')

router.get('/', function(req, res, next) {
  getOathToken().then(token=>{
    console.log(token)
    // c2bRegisterUrl(token).then((response)=> {
    //   console.log(response);
    // }).catch((error)=> {
    //   console.log('register url ', error);
    // })

    // c2bSimulateUrl(token).then((response) => {
    //   console.log('simulate payment: ', response);
    // }).catch((error) => {
    //   console.log('simulate url ', error);
    // })

    mPesaSTKPush(token).then(response=>console.log(response)).catch(error=>console.log(error))
    res.render('index', { title: 'Ticket Processor System', auth: token });
  }).catch(error=>{
    console.log('Generate token ', error);
  })
});

module.exports = router;

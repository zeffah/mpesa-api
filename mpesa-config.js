require('dotenv').config()
const request = require('request')
const BASE_URL = process.env.BASE_URL
const CONSUMER_KEY = process.env.CONSUMER_KEY
const CONSUMER_SECRET = process.env.CONSUMER_SECRET
const TOKEN_GENERATE_URL = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials"
const REGISTER_URL = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl"
const SIMULATE_URL = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate"
const CONFIRMATION_URL = BASE_URL+"/confirmation"
const VALIDATION_URL = BASE_URL+"/validation"
const STK_PUSH_URL = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest"
const AUTH = "Basic " + new Buffer.from(CONSUMER_KEY + ":" + CONSUMER_SECRET).toString("base64")

const getOathToken = ()=> {
    let reqBody = {
        url:TOKEN_GENERATE_URL,
        headers: {
            "Authorization": AUTH,
            ContentType: 'application/json'
        }
    }
    console.log(BASE_URL)
    return new Promise((resolve, reject) => {
        request(reqBody, (err, res, body) => {
            if (err)return reject(err)
            try{
                // JSON.parse() can throw an exception if not valid JSON
                resolve(JSON.parse(body).access_token);
            }catch(e){
                reject(e)
            }
        })
    })
}

const c2bRegisterUrl = (OATH_TOKEN) => {
    let AUTH = "Bearer " + OATH_TOKEN;
    return new Promise((resolve, reject) => {
        request({
            method: 'POST',
            url: REGISTER_URL,
            headers: {
                "Authorization": AUTH,
                ContentType: 'application/json'
            }, json: {
                ShortCode: "600779",
                ResponseType: "Completed",
                ConfirmationURL: CONFIRMATION_URL,
                ValidationURL: VALIDATION_URL
            }
        }, (err, res, body) => {
            if (err) return reject(err)
            try {
                resolve(body);
            } catch (e) {
                reject(e)
            }
        })
    })
}

const c2bSimulateUrl = (OATH_TOKEN) => {
    let AUTH = "Bearer " + OATH_TOKEN;
    return new Promise((resolve, reject) => {
        request({
            method: 'POST',
            url: SIMULATE_URL,
            headers: {
                "Authorization": AUTH,
                ContentType: 'application/json'
            }, json: {
                "ShortCode": "600779",
                "CommandID": "CustomerPayBillOnline",
                "Amount": "500",
                "Msisdn": "254708374149",
                "BillRefNumber": "Account"
            }
        }, (err, res, body) => {
            if (err) return reject(err)
            try {
                resolve(body);
            } catch (e) {
                reject(e)
            }
        })
    })
}

const mPesaSTKPush = (OATH_TOKEN) => {
    let AUTH = "Bearer " + OATH_TOKEN;
    console.log('OATH: ', OATH_TOKEN)
    console.log('phone: '+process.env.TEST_PHONE_NUMBER)
    let password = new Buffer.from("174379bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c91920181015202459").toString("base64")
    console.log('PASSWORD: ', password)
    return new Promise((resolve, reject) => {
        request({
                method: 'POST',
                url: STK_PUSH_URL,
                headers: {
                    "Authorization": AUTH,
                    ContentType: 'application/json'
                },
                json: {
                    "BusinessShortCode": "174379",
                    "Password": password,
                    "Timestamp": "20181015202459",
                    "TransactionType": "CustomerPayBillOnline",
                    "Amount": "10",
                    "PartyA": process.env.TEST_PHONE_NUMBER,
                    "PartyB": "174379",
                    "PhoneNumber": process.env.TEST_PHONE_NUMBER,
                    "CallBackURL": CONFIRMATION_URL,
                    "AccountReference": "Account",
                    "TransactionDesc": "Payment stk Test"
                }
            },
            (error, response, body) => {
                if (error) return reject(error)
                try {
                    resolve(body)
                } catch (e) {
                    reject(e)
                }
            }
        )
    })
}

module.exports = {
    getOathToken, 
    c2bRegisterUrl,
    c2bSimulateUrl,
    mPesaSTKPush
}